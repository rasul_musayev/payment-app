package az.ingress.common.security.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomSpringSecurityUser extends User {

    private static final long serialVersionUID = 3522416053866116034L;

    @Getter
    @Setter
    private String organisation;

    public CustomSpringSecurityUser(String email, String password,
                                    Collection<? extends GrantedAuthority> authorities, String organisation) {
        super(email, password, authorities);
        this.organisation = organisation;
    }
}
