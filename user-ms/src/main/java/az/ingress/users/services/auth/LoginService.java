package az.ingress.users.services.auth;

import com.api.first.development.openapi.model.SignInRequest;
import com.api.first.development.openapi.model.SignInResponse;

public interface LoginService {

    SignInResponse signIn(SignInRequest body);
}
