package az.ingress.users.services.auth;

import az.ingress.common.security.auth.services.JwtService;
import az.ingress.common.security.config.SecurityProperties;
import com.api.first.development.openapi.model.SignInRequest;
import com.api.first.development.openapi.model.SignInResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final JwtService jwtService;
    private final SecurityProperties securityProperties;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    @Override
    public SignInResponse signIn(SignInRequest body) {
        log.info("Authentication requested by {}", body.getUsername());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(body.getUsername(),
                body.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        log.trace("Sign in result is {}", authentication);

        String accessToken = jwtService.issueToken(authentication, Duration.ofSeconds(securityProperties
                .getJwtProperties()
                .getTokenValidityInSeconds()));
        SignInResponse signInResponse = SignInResponse
                .builder()
                .accessToken(accessToken)
                .refreshToken("---")
                .build();
//        RefreshTokenDto refreshTokenDto = issueRefreshToken(authentication);
//        return new TokenRefreshResponseDto(jwtAccessTokenDto, refreshTokenDto);
        return signInResponse;
    }
}
