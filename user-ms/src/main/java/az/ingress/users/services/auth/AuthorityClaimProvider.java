package az.ingress.users.services.auth;

import az.ingress.common.security.auth.services.ClaimSet;
import az.ingress.common.security.auth.services.ClaimSetProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

import static az.ingress.common.security.auth.services.TokenAuthService.AUTHORITIES_CLAIM;

@Component
public class AuthorityClaimProvider implements ClaimSetProvider {

    @Override
    public ClaimSet provide(Authentication authentication) {
        return new ClaimSet(AUTHORITIES_CLAIM, getAuthorities(authentication));
    }

    private Set<String> getAuthorities(Authentication authentication) {
        return authentication.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }
}
