package az.ingress.users.services;

import az.ingress.common.exception.ApplicationException;
import az.ingress.users.entity.User;
import az.ingress.users.errors.Errors;
import az.ingress.users.repository.UserRepository;
import com.api.first.development.openapi.model.UserRequest;
import com.api.first.development.openapi.model.UserResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private AtomicLong userCounter = new AtomicLong();// 1 1 2 -> 2

    private volatile long count = 10L;

    @Override
    public UserResponse createUser(UserRequest body) {
        userCounter.incrementAndGet();//1 2 3
        final User user = modelMapper.map(body, User.class);
        user.setPassword(passwordEncoder.encode(body.getPassword()));
        return modelMapper.map(userRepository.save(user), UserResponse.class);
    }

    @Override
    public void deleteUser(Long id) {
        User user = validateAndGetUser(id);
        userRepository.delete(user);
    }

    @Override
    public UserResponse getUser(Long id) {
        return modelMapper.map(validateAndGetUser(id), UserResponse.class);
    }

    @Override
    public List<UserResponse> listUsers(Integer page, Integer size) {
        final PageRequest pageRequest = PageRequest.of(page, size);
        return userRepository.findAll(pageRequest)
                .stream()
                .map((user) -> modelMapper.map(user, UserResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse updateUser(Long id, UserRequest body) {
        validateAndGetUser(id);
        final User user1 = modelMapper.map(body, User.class);
        user1.setId(id);
        return modelMapper.map(userRepository.save(user1), UserResponse.class);
    }

    @Override
    public void activateAccount(Long userId) {
        final User user = validateAndGetUser(userId);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        userRepository.save(user);
    }

    private User validateAndGetUser(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ApplicationException(Errors.USER_NOT_FOUND, Map.of("id", id)));
    }

}
