package az.ingress.users.services;

import com.api.first.development.openapi.model.UserRequest;
import com.api.first.development.openapi.model.UserResponse;

import java.util.List;

public interface UserService {

    UserResponse createUser(UserRequest body);

    void deleteUser(Long id);

    UserResponse getUser(Long id);

    List<UserResponse> listUsers(Integer page, Integer size);

    UserResponse updateUser(Long id, UserRequest body);

    void activateAccount(Long userId);
}
