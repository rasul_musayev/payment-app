package az.ingress.users.rest;

import az.ingress.users.services.auth.LoginService;
import com.api.first.development.openapi.api.AuthApi;
import com.api.first.development.openapi.model.SignInRequest;
import com.api.first.development.openapi.model.SignInResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AuthController implements AuthApi {

    private final LoginService loginService;

    @GetMapping("/home")
    public String signIn(Authentication authentication) {
        log.info("Authentication :{}", authentication);
        log.info("User Principal :{}", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "Hello World, user is " + authentication.getPrincipal();
    }

    @Override
    public ResponseEntity<SignInResponse> signIn(SignInRequest body) {
        return ResponseEntity.ok(loginService.signIn(body));
    }
}
