package az.ingress.users.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/welcome")
public class Welcome {

    @GetMapping
    public String sayWelcome() {
        return "Welcome to user ms + 1.3 version";
    }
}
