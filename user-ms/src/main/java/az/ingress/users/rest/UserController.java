package az.ingress.users.rest;

import az.ingress.users.services.UserService;
import com.api.first.development.openapi.api.UserApi;
import com.api.first.development.openapi.model.UserRequest;
import com.api.first.development.openapi.model.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;

    @Override
    public ResponseEntity<Void> activateUser(Long userId) {
        userService.activateAccount(userId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<UserResponse> createUser(@Validated UserRequest body) {
        return ResponseEntity.ok(userService.createUser(body));
    }

    @Override
    public ResponseEntity<Void> deleteUser(Long id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<UserResponse> getUser(Long id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @Override
    public ResponseEntity<List<UserResponse>> listUsers(Integer page, Integer size) {
        return ResponseEntity.ok(userService.listUsers(page, size));
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(Long id, UserRequest body) {
        return ResponseEntity.ok(userService.updateUser(id, body));
    }
}
