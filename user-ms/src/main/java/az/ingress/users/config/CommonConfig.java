package az.ingress.users.config;

import az.ingress.common.config.LocaleMessageSourceConfig;
import az.ingress.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({GlobalExceptionHandler.class, LocaleMessageSourceConfig.class})
public class CommonConfig {
}
