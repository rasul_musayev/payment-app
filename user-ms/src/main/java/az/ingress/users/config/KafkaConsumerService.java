package az.ingress.users.config;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
//@Service
public class KafkaConsumerService {

    @KafkaListener(
            topics = "user-activation-events",
            groupId = "course-ms",
            containerFactory = "kafkaListenerContainerFactory")
    public void onEvent(String message) {
        log.trace("Received message from kafka in consumer group course-ms c1 {}", message);
    }

    @KafkaListener(
            topics = "user-activation-events",
            groupId = "course-ms",
            containerFactory = "kafkaListenerContainerFactory")
    public void onEvent1(String message) {
        log.trace("Received message from kafka in consumer group course-ms c2 {}", message);
    }

    public static Logger getLog() {
        return log;
    }
}
