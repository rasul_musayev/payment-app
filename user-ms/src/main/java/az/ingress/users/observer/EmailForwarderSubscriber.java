package az.ingress.users.observer;

public class EmailForwarderSubscriber implements Subscriber {
    @Override
    public void consume(Message message) {
        System.out.println("Forwarding email to other people :" + message);
    }
}
