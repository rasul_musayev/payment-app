package az.ingress.users.observer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Message {
    private String email;

    private String subject;

    private String message;

}
