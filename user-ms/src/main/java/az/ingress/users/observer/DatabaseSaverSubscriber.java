package az.ingress.users.observer;

public class DatabaseSaverSubscriber implements Subscriber {
    @Override
    public void consume(Message message) {
        System.out.println("Saving message into database " + message);
    }
}
