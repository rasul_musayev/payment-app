package az.ingress.users.observer;

public class Main {
    public static void main(String[] args) {
        Subscriber subscriber = new EmailForwarderSubscriber();
        Subscriber subscriber2 = new EmailForwarderSubscriber();
        Subscriber subscriber3 = new EmailForwarderSubscriber();

        Publisher publisher = new EmailSenderPublisher();
        Message message = new Message("info@ingress.az", "Test,", "Test");
        publisher.processEmail(message);
        System.out.println("=======================");
        publisher.subscribe(subscriber);
        publisher.subscribe(subscriber2);

        publisher.processEmail(message);
        System.out.println("=======================");
        publisher.subscribe(subscriber3);

        publisher.processEmail(message);
        System.out.println("=======================");

        publisher.unSubscribe(subscriber);
        publisher.processEmail(message);
    }
}
