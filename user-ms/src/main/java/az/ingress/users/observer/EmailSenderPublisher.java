package az.ingress.users.observer;

import java.util.HashSet;
import java.util.Set;

public class EmailSenderPublisher implements Publisher {
    private Set<Subscriber> consumers = new HashSet<>();

    @Override
    public void subscribe(Subscriber subscriber) {
        consumers.add(subscriber);
    }

    @Override
    public void unSubscribe(Subscriber consumer) {
        consumers.remove(consumer);
    }

    public void processEmail(Message message) {
        System.out.println("Generated new message:" + message);
        System.out.println("Processing finished and notifying consumers: " + message);
        for (Subscriber consumer : consumers) {
            consumer.consume(message);
        }
    }
}
