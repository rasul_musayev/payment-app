package az.ingress.users.observer;

public interface Publisher {
    void subscribe(Subscriber subscriber);

    void unSubscribe(Subscriber subscriber);

    void processEmail(Message message);
}
