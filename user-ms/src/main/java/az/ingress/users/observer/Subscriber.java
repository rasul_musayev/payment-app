package az.ingress.users.observer;

public interface Subscriber {
    void consume(Message message);
}
