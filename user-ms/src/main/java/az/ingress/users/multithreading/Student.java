package az.ingress.users.multithreading;

import java.util.ArrayList;
import java.util.List;

public final class Student {

    private final Long id;
    private final StringBuilder firstName;
    private final StringBuilder lastName;
    private final List<Account> accounts;

    public Student(Long id, StringBuilder firstName, StringBuilder lastName, List<Account> accounts) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    }

    public Long getId() {
        return id;
    }

    public List<Account> getAccounts() {
        List list = new ArrayList(accounts);
//        List<Account> list = new CopyOnWriteArrayList();
//        list.addAll(accounts);
        return list;
    }

    public StringBuilder getFirstName() {
        return new StringBuilder(firstName);
    }

    public StringBuilder getLastName() {
        return new StringBuilder(lastName);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName=" + firstName +
                ", lastName=" + lastName +
                ", accounts=" + accounts +
                '}';
    }
}
