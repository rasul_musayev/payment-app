package az.ingress.users.multithreading;

public class TestSingleton {
    private static TestSingleton instance;

    public static TestSingleton getInstance() {
        synchronized (TestSingleton.class) {
            if (instance == null)
                instance = new TestSingleton();
            return instance;
        }
    }
}
