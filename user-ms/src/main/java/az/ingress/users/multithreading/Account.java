package az.ingress.users.multithreading;

import lombok.SneakyThrows;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class Account extends Object {
    static Account account = new Account();
    private Integer balance =25;

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                '}';
    }

    public static void main(String[] args) throws InterruptedException {
        Runnable withdraw = () -> account.withDraw(200);
        Runnable withdraw1 = () -> account.withDraw(150);
        Runnable withdraw2 = () -> account.withDraw(100);

        Runnable deposit = () -> account.deposit(95);

        Lock lock = new ReentrantLock();
        lock.tryLock();

//        Runnable runnable1 = account::withDraw;

        Thread thread1 = new Thread(withdraw);
        Thread thread4 = new Thread(withdraw1);
        Thread thread3 = new Thread(withdraw2);


        Thread thread2 = new Thread(deposit);
        System.out.println("Deposited");

//        Thread thread3 = new Thread(runnable);
//        Thread thread4 = new Thread(runnable1);


        thread1.start();
        thread3.start();
        thread4.start();

        sleep(2000);
        thread2.start();

        thread1.join();
        thread3.join();
        thread3.join();
        thread4.join();
        System.out.println("Main ended" + account.balance);
    }

    @SneakyThrows
    private void deposit(int amount) {
        synchronized (account) {
            balance = balance + amount;
            notifyAll();
        }
    }
//        synchronized (Account.class) {
//            System.out.println("Thread in " + Thread.currentThread().getName());
//            int newBalance = balance;
//            Thread.sleep(100);
//            balance = newBalance + amount;
//            System.out.println("Thread out  " + Thread.currentThread().getName());
//        }

    @SneakyThrows
    private void withDraw(int amount) {
        synchronized (account) {
            while (amount > balance) {    //wait sleep
                System.out.println(Thread.currentThread().getName() + " - Insufficient balance waiting, balance = " + balance + " amount = " + amount);
                sleep(100000);   //not releases monitor
            }
            System.out.println(Thread.currentThread().getName() + " - succes continue witdraw, balance = " + balance + " amount = " + amount);
            balance = balance - amount;

        }
    }


//    private static String name = "Test";
//
//    public static void main(String[] args) throws InterruptedException {
//        System.out.println("This is main thread " + Thread.currentThread().getName());
//        System.out.println("Account" + name);
//        StringBuilder name = new StringBuilder("Test");
//        Runnable runnable = () -> method1(name);
//        Thread thread = new Thread(runnable);
//        Thread thread2 = new Thread(runnable);
//        thread2.start();
//        thread.setName("Test");
//        thread.start();
//        thread.join();
//        System.out.println("Account ended" + name);
//    }
//
//    private static void method1(StringBuilder name) {
//        System.out.println("method1" + name);
//        method2();
//        name.append("Test-----");
//        System.out.println("method1" + name);
//    }
//
//    @SneakyThrows
//    private static void method2() {
//        System.out.println("method2");
//        System.out.println("Method 2 in action");
//        sleep(3000);
//        if (1 == 1) {
//            throw new RuntimeException("SOMETHING BAD HAPPEND");
//        }
//        System.out.println("Method 2 in action");
//        System.out.println("method2");
//    }
}
