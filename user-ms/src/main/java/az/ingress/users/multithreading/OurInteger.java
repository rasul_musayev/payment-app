package az.ingress.users.multithreading;

public class OurInteger {

    public int value;

    public void  setValue(int value) {
        synchronized (this) {
            this.value = value;
        }
    }
}
