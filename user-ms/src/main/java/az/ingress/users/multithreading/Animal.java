package az.ingress.users.multithreading;

import lombok.SneakyThrows;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Animal implements Runnable {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        makeNoise();
    }

    @SneakyThrows
    public synchronized void makeNoise() {
        synchronized (this) {
            int i = 0;
            while (true) {
                System.out.println(name + "- making noise" + i++);
                Thread.sleep(1000);
            }
        }
    }

    public static void main(String[] args) {
        Animal dog = new Animal("Dog");
        Animal cat = new Animal("Cat");

        ExecutorService executors = Executors.newFixedThreadPool(3);
        executors.submit(dog);
        executors.submit(cat);
    }


}
