package az.ingress.users.multithreading;

import lombok.SneakyThrows;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main2 {
    static UUID uuid;

    @SneakyThrows
    public static void main(String[] args) {

        Callable callable = () -> UUID.randomUUID();

        Callable callable1 = () -> {

            System.out.println("Thread is running " + Thread.currentThread().getId());

            try {
                System.out.println("Thread is sleeping " + Thread.currentThread());
                Thread.sleep(1000);
            } catch (Exception e) {
                // log write
            }
            System.out.println("Thread is ending " + Thread.currentThread());
            return UUID.randomUUID();
        };
        Callable callable2 = () -> {
            System.out.println("Thread is running " + Thread.currentThread().getId());

            try {
                System.out.println("Thread is sleeping " + Thread.currentThread());
                Thread.sleep(1000);
            } catch (Exception e) {
                // log write
            }
            System.out.println("Thread is ending " + Thread.currentThread());
            return UUID.randomUUID();
        };

        ExecutorService service = Executors.newFixedThreadPool(10);
        final Future submit = service.submit(callable1);//thread1
        final Future submit1 = service.submit(callable1);//thread2

        System.out.println(submit1.get());
        System.out.println(submit.get());

//        Thread thread = new Thread(runnable);
//        thread.start();
//        thread.join();
//
//        Thread thread2 = new Thread(runnable);
//        thread2.start();
//        thread2.join();

//        System.out.println(uuid);
    }
}
