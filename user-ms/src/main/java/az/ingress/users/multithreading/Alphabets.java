package az.ingress.users.multithreading;

import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Alphabets {

    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    @SuppressWarnings("rawtypes")
    public static void main(String[] args) {
        BlockingQueue<String> alph = new ArrayBlockingQueue<>(7);
        BlockingQueue<String> counter = new ArrayBlockingQueue<>(7);
        List<String> list = List.of("Tural", "Atilla", "Abdulla", "Rasul");


        list.stream()
                .map((s) -> {
                    s = s.toUpperCase();
                    System.out.println();
                    return s;
                }).map((s) -> {
                    s = s + "-" + s.length();
                    System.out.println(s);
                    return s;
                }).collect(Collectors.toList());

        Runnable producer = () -> {
            System.out.println("String producer callable started");
            for (String string : list) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("Producing string");
                try {
                    alph.put(string);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        Runnable consumer = () -> {
            while (true) {
                System.out.println("Callable upper case started");
                String s = null;
                try {
                    s = alph.take();
                    s = s.toUpperCase();
                    System.out.println(s);
                    counter.put(s + "-" + s.length());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        Runnable consumer2 = () -> {
            while (true) {
                System.out.println("Callable upper case started");
                String s = null;
                try {
                    s = counter.take();
                    System.out.println(s);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
//        executorService.submit(consumer);
//        executorService.submit(producer);
//        executorService.submit(consumer2);
    }

}
