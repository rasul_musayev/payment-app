package az.ingress.users.multithreading;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main3 {

    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
        int[] nums = {10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                200, 25, 78, 88, 65, 34, 89, 99, 205, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                200, 25, 78, 88, 65, 34, 89, 99, 205, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                200, 25, 78, 88, 65, 34, 89, 99, 205, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,
                10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 95, 10, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 9510, 16, 27, 100, 89, 95,};
        long current = System.currentTimeMillis();
        //ForkJoinPool customThreadPool = new ForkJoinPool(1);
        Arrays.stream(nums).parallel().mapToObj(Main3::calculateFactorial).collect(Collectors.toList());
        System.out.println("Elapsed: " + (System.currentTimeMillis() - current));
    }

    private static BigInteger calculateFactorial(int a) {
        BigInteger factorial = BigInteger.valueOf(1);
        for (int i = 1; i <= a; i++)
            factorial = factorial.multiply(BigInteger.valueOf(i));
        return factorial;
    }
}
