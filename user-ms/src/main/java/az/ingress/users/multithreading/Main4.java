package az.ingress.users.multithreading;

import lombok.SneakyThrows;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public class Main4 {
    //MUTEX -> "mutual exclusion"
    @SneakyThrows
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(20);
        List<Integer> list = List.of(100, 200, 700, 89, 9000, 10, 25, 890, 787,686);//100
        CountDownLatch countDownLatch = new CountDownLatch(list.size());
        //this  ArrayList not "Thread safe"
        List<Future<BigInteger>> futures = new ArrayList<>();
        System.out.println("Calculating factorials....");

        for (Integer value : list) {
            final Future<BigInteger> future = service.submit(() -> bigInteger(value, countDownLatch));
            futures.add(future);
        }
        countDownLatch.await();
        System.out.println("Calculating sum....");
        BigInteger sum = BigInteger.valueOf(0);
        for (Future<BigInteger> future : futures) {
            sum = sum.add(future.get());
        }
        System.out.println("Sum is " + sum);
        System.out.println(Runtime.getRuntime().availableProcessors());//16

    }

    //        Lock lockA = new ReentrantLock();
//        CountDownLatch countDownLatch = new CountDownLatch(3);
//
//        Callable callable = () -> bigInteger(100, countDownLatch);
//        Callable callable2 = () -> bigInteger(100, countDownLatch);
//
//        ExecutorService service = Executors.newFixedThreadPool(50);
//        final Future<BigInteger> submit = service.submit(callable);
//        service.submit(callable2);
//        service.submit(callable);
//        Thread.sleep(1000);
//
//        System.out.println("Waiting for completion" + countDownLatch.getCount());
//        countDownLatch.await();
//        System.out.println("All done!");
//
////        final BigInteger o = submit.get();
////        System.out.println("Main printed the result as well : " + o);
//    }
//
    @SneakyThrows
    private static BigInteger bigInteger(long calculateFactorial, CountDownLatch countDownLatch) {
        System.out.println("Calculating factorials of : " + calculateFactorial);
        BigInteger bigInteger = BigInteger.valueOf(1);
        for (long i = 0; i < calculateFactorial; i++) {
            bigInteger = bigInteger.multiply(BigInteger.valueOf(i));
        }
        countDownLatch.countDown();
        System.out.println("Calculating factorials done : " + calculateFactorial + " =" + bigInteger);
        return bigInteger;
    }

    private static void call2(Lock lock) {
        System.out.println("Something happend here");
        lock.unlock();
        System.out.println("Normal unconccurent operation");
    }
}
