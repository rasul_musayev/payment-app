package az.ingress.users.multithreading;


import java.util.ArrayList;
import java.util.List;

public class Main {
//    private final Student student = new Student();

    public static void main(String[] args) {
//        Main main = new Main();
//
//        Runnable runnable = () -> {
//            main.populateObject(1L, "Abdulla", "Maxsudov");
//        };
//        Runnable runnable2 = () -> {
//            main.populateObject(2L, "Ruhani", "Aliyev");
//        };
//        Runnable runnable3 = () -> {
//            main.populateObject(3L, "Rasul", "Musayev");
//        };
//
//        Thread thread1 = new Thread(runnable);
//        Thread thread2 = new Thread(runnable2);
//        Thread thread3 = new Thread(runnable3);
//
//        thread1.start();
//        thread2.start();
//        thread3.start();

        Account account = new Account();
        account.setBalance(100);
        List<Account> list = new ArrayList<>();
        list.add(account);
        Student student = new Student(1L, new StringBuilder("Rasul"), new StringBuilder("Musayev"), list);

        System.out.println(student);
        student.getAccounts().get(0).setBalance(200);

        student.getFirstName().append("djsbkdsbk");
        System.out.println(student);

        student.getAccounts().add(new Account());
        System.out.println(student);

        String q;//immutable
        StringBuilder stringBuilder;//mutable and not "Thread Safe"
        StringBuffer stringBuffer;//mutable and "Thread Safe"

        String s = "Hello";
        System.out.println(s.concat(" World")); //Hello World
        System.out.println(s);//Hello
//        OurInteger a = new OurInteger(9);
//        increment(a);
//        System.out.println("in main" + a.value);  //9
    }

    private static void increment(OurInteger b) { //OurInteger not Thread Safe
        b.value = b.value + 1;
        System.out.println("in method " + b.value);//10
    }

    private void populateObject(Long id, String fristName, String lastName) {

//        saveObjects(student);
    }

    private void saveObjects(Student student) {
        System.out.println("Saved object : " + student);
    }
}
