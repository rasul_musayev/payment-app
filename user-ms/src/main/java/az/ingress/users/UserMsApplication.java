package az.ingress.users;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class UserMsApplication implements CommandLineRunner {

//    private final KafkaTemplate<String, String> kafkaTemplate;

    public static void main(String[] args) {
        SpringApplication.run(UserMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // for (int i = 0; i < 100; i++)
//        while (1 == 1) {
//            Thread.sleep(1000);
//            long current = System.currentTimeMillis();
//
//            kafkaTemplate.setProducerListener(new ProducerListener<String, String>() {
//                @Override
//                public void onSuccess(ProducerRecord<String, String> producerRecord, RecordMetadata recordMetadata) {
//                    log.info("Message written into kafka {} {}", producerRecord, recordMetadata);
//                }
//            });
//
//            try {
//                //final SendResult<String, String> stringStringSendResult = kafkaTemplate.send("user-activation-events", "key", "Hello Kafka ").get();
//                kafkaTemplate.send("user-activation-events", "Hello Kafka " + UUID.randomUUID());
//                System.out.println("Sending result");
//            } catch (Exception e) {
//                log.error("Failed to write, retrying...");
//            }
//            System.out.println("Elapsed time:" + (System.currentTimeMillis() - current));
//        }
    }
}
