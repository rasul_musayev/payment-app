package az.ingress.notifications.config;

import az.ingress.common.security.auth.JwtAuthFilterConfigurerAdapter;
import az.ingress.common.security.auth.JwtAuthRequestFilter;
import az.ingress.common.security.auth.services.JwtService;
import az.ingress.common.security.auth.services.TokenAuthService;
import az.ingress.common.security.config.ApplicationSecurityConfig;
import az.ingress.common.security.config.BaseSecurityConfig;
import az.ingress.common.security.config.SecurityProperties;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@Configuration
@Import({BaseSecurityConfig.class, SecurityProperties.class,
        JwtAuthFilterConfigurerAdapter.class, JwtAuthRequestFilter.class,
        TokenAuthService.class, JwtService.class})
public class SecurityConfig implements ApplicationSecurityConfig {

    @Override
    @SneakyThrows
    public void configure(HttpSecurity http) {
        http.authorizeRequests()
                .antMatchers(
                        "/welcome")
                .permitAll()
                .antMatchers("/home")
                .hasAnyRole("ADMIN");
    }
}
