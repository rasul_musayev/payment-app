package az.ingress.notifications.email;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
//@Component
@RequiredArgsConstructor
public class UserActivationEmailsConsumer {

    private final SendMailService sendMailService;
    private final KafkaConsumer<String, String> consumer;
    private ExecutorService executorService;

    public void initExecutor() {
        executorService = Executors.newFixedThreadPool(100);
    }

//    @KafkaListener(topics = "user-activation-events", groupId = "notification-ms", containerFactory = "kafkaListenerContainerFactory")
//    public void onEvent(String message) {
//        log.trace("Received message from kafka in consumer group notification-ms c1 {}", message);
//        sendMailService.send("rasul.musayev.15@gmail.com", "Test Notification ms", message);
//    }

//        @KafkaListener(id = "id0", topicPartitions = {@TopicPartition(topic = "user-activation-events", partitions = {"0"})})
//    public void listenPartition0(ConsumerRecord<?, ?> record) {
//        System.out.println("Listener Id0, Thread ID: " + Thread.currentThread().getId());
//        System.out.println("Received: " + record);
//    }
//
//    @KafkaListener(id = "id1", topicPartitions = {@TopicPartition(topic = "user-activation-events", partitions = {"1"})})
//    public void listenPartition1(ConsumerRecord<?, ?> record) {
//        System.out.println("Listener Id1, Thread ID: " + Thread.currentThread().getId());
//        System.out.println("Received: " + record);
//    }

    //    @KafkaListener(id = "id2", topicPartitions = {@TopicPartition(topic = "user-activation-events", partitions = {"2"})})
//    public void listenPartition2(ConsumerRecord<?, ?> record) {
//        System.out.println("Listene   r Id2, Thread ID: " + Thread.currentThread().getId());
//        System.out.println("Received: " + record);
//    }
    @PostConstruct
    @SneakyThrows
    public void initConsumer() {
        System.out.println("Initating consumer");
        initExecutor();
        consumer.subscribe(Arrays.asList("user-activation-events"));
        AtomicInteger counter = new AtomicInteger(0);
        long initialTime = System.currentTimeMillis();
        while (counter.get() < 4247) {
            ConsumerRecords<String, String> records = consumer.poll(4247);
            for (ConsumerRecord<String, String> record : records) {
                EmailProcessor emailProcessor = new EmailProcessor(consumer, record, counter);
                executorService.submit(emailProcessor);
                System.out.println("offset" + record.offset() + "partition " + record.partition());
            }
        }
        Map<TopicPartition, OffsetAndMetadata> commitOffset = new HashMap<>();
        TopicPartition topicPartition = new TopicPartition("user-activation-events", 0);
        OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(503);
        commitOffset.put(topicPartition, offsetAndMetadata);
        System.out.println("Committing offset " + commitOffset);
        consumer.commitSync(commitOffset);
        System.out.println("Committed");
        System.out.println("Elapsed :" + (System.currentTimeMillis() - initialTime));
    }
}
