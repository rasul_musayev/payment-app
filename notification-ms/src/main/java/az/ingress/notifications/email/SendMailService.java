package az.ingress.notifications.email;


public interface SendMailService {
    void send(String sendTo, String subject, String htmlBody);
}
